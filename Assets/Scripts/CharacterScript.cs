﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterScript : MonoBehaviour{

    public Color couleur;

    public float grabDistance;
    public float speed;
    public float speedRotation;
    public GameObject objectDetector;
    public GameObject handsSpot;
    public GameObject raycastSpot;
    private GameObject _boxInHand;
    

    public static Action onObjectPicked;


    // Update is called once per frame
    void Update(){
        //Diriger mon Character temporaire
        if (Input.GetKey(KeyCode.UpArrow)){
            transform.Translate(new Vector3(0, 0, 1) * speed * Time.deltaTime, Space.Self);
        }
        else if (Input.GetKey(KeyCode.DownArrow)){
            transform.Translate(new Vector3(0, 0, -1) * speed * Time.deltaTime, Space.Self);
        }
        if (Input.GetKey(KeyCode.RightArrow)){
            transform.Rotate(new Vector3(0, 1, 0) * speedRotation * Time.deltaTime, Space.Self);
        }
        else if (Input.GetKey(KeyCode.LeftArrow)){
            transform.Rotate(new Vector3(0, -1, 0) * speedRotation * Time.deltaTime, Space.Self);
        }
        // Attraper un objet
        if (Input.GetKeyDown(KeyCode.G)){
            if (!_boxInHand){
                //Détecter le block que l'on a en face de soi.
                GameObject box = GetItemDetected();
                if (box){
                    _boxInHand = box;
                    Physics.IgnoreCollision(_boxInHand.GetComponent<Collider>(), GetComponent<Collider>(), true);
                    _boxInHand.GetComponent<PickableItemScript>().target = handsSpot;
                    _boxInHand.GetComponent<PickableItemScript>().TakeIt();
                }
            }
            else{
                _boxInHand.GetComponent<MeshRenderer>().material.color = couleur;
                //_boxInHand.GetComponent<PickableItemScript>().DropIt();
                _boxInHand = null;
                Physics.IgnoreCollision(_boxInHand.GetComponent<Collider>(), GetComponent<Collider>(), false);
            }
        }
        if (Input.GetKeyDown(KeyCode.R)){
            _boxInHand.transform.localPosition = Vector3.zero;
            _boxInHand.transform.localRotation = Quaternion.Euler(0,90,0);
        }

    }

    private GameObject GetItemDetected(){
        if (objectDetector.GetComponent<DetectionScript>().boxesDetected.Count > 0)
            return objectDetector.GetComponent<DetectionScript>().boxesDetected[0];
        else
            return null;
    }

}
