﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    private AudioSource _audioSource;

    private static AudioManager instance = null;
    public static AudioManager Instance{
        get { return instance; }
    }

    private void Awake(){
        if (instance != null && instance != this){
            Destroy(this.gameObject);
            return;
        }
        else{
            instance = this;
            if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("menu"))
                MenuManager.mm.audioManager = instance;
        }
        DontDestroyOnLoad(gameObject);
        _audioSource = GetComponent<AudioSource>();
    }

    [Header("Menu")]
    public AudioSource selectSound;
    public AudioSource validSound;
    [Header("Splashscreen")]
    public AudioSource splashscreenSound;
    [Header("Ambiance")]
    public AudioSource constructionSiteSound;
    [Header("Camion")]
    public AudioSource klaxonSound;
    public AudioSource moteurSound;
    public AudioSource depotSound;
    [Header("ObjetsConstruction")]
    public AudioSource poutreSound;
    [Header("Character")]
    public AudioSource pasSound;
    public AudioSource atterrissageSound;
    public AudioSource attraperObjetSound;
    public AudioSource jetpackSound;


    public void Play(AudioSource sound){
        sound.Play();
    }

    public void Stop(AudioSource sound){
        sound.Stop();
    }

}
