﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionScript : MonoBehaviour{

    public List<GameObject> boxesDetected;

    private void OnTriggerEnter(Collider other){
        if(other.tag == "Pickable"){
            if (!boxesDetected.Contains(other.gameObject)){
                boxesDetected.Add(other.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other){
        if (other.tag == "Pickable"){
            if(boxesDetected.Contains(other.gameObject))
                boxesDetected.Remove(other.gameObject);
        }
    }

}
