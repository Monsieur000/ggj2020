﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
	public static GameManager Instance;
	public static bool Exists => Instance != null;

	[Header("Camera")]
	[SerializeField]CameraController _mainCamera;
	[SerializeField]float _scaleFactor;
	[SerializeField]float _moveFactor;
	[SerializeField] float _minCameraSize;
	[SerializeField] float _maxCameraSize;
	[SerializeField] float _minCameraHeight;
	[SerializeField] float _maxCameraHeight;
	[SerializeField] bool _scaleForPlayers;
	private float _currentMaxHeight;

	[Header("Raycaster")]
	[SerializeField] private LayerMask itemLayerMask;
	[SerializeField] private LayerMask playerLayerMask;
	[SerializeField] Transform _raycaster;

	[Header("UI")]
	[SerializeField] private TextMeshProUGUI _uiTowerHeight;
	[SerializeField] private TextMeshProUGUI _uiTowerMaxHeight;
	[SerializeField] private TextMeshProUGUI _uiTimer;

	[Header("Tower")]
	[SerializeField] float _minTowerHeight;
	[SerializeField] float _maxTowerHeight;
	private float _currentTowerHeight;
	private float _maxHeight;

	[Header("Spawn")]
	[SerializeField]List<Transform> _spawnPoints;
    private InputManager _inputManager = null;
    [SerializeField] private WorkerControl[] _playerWorkers = null;

    [Header("Truck")]
	[SerializeField] TruckScript _truck;
	[SerializeField] TriggerButton _truckOpener;
	[SerializeField] GameObject _truckAlertSign;
	[SerializeField] List<Transform> _beamPositions;
	[SerializeField] List<Transform> _boxPositions;
	[SerializeField] List<Transform> _boardPositions;
	[SerializeField] float _truckRunDelay = 30f;
	[SerializeField] int _minItemToDrop = 1;
	[SerializeField] int _maxItemToDrop = 7;
	[SerializeField] int _maxItemTotal = 50;
	private float _lastTruckRun;

	[Header("Prefabs")]
	[SerializeField]GameObject _playerPrefab;
	[SerializeField]GameObject _beamPrefab;
	[SerializeField]List<GameObject> _boxPrefab;
	[SerializeField]GameObject _boardPrefab;

	private Dictionary<Transform, bool> _playerSlots;
	private Dictionary<Transform, Transform> _beamStock;
	private Dictionary<Transform, Transform> _boxStock;
	private Dictionary<Transform, Transform> _boardStock;
	private HashSet<Transform> _allItems;

	[Header("Audio")]
	public AudioManager audioManager;

	private void Awake()
	{
		Instance = this;
		_playerSlots = new Dictionary<Transform, bool>();
		_beamStock = new Dictionary<Transform, Transform>();
		_boardStock = new Dictionary<Transform, Transform>();
		_boxStock = new Dictionary<Transform, Transform>();
		_allItems = new HashSet<Transform>();
		audioManager = AudioManager.Instance;
	}

	// Start is called before the first frame update
	private IEnumerator Start()
    {
        _inputManager = InputManager.Instance;

        foreach(Transform spawnPoint in _spawnPoints)
		{
			_playerSlots.Add(spawnPoint, false); 
		}

		foreach(Transform beam in _beamPositions)
		{
			_beamStock.Add(beam, null);
		}

		foreach (Transform board in _boardPositions)
		{
			_boardStock.Add(board, null);
		}

		foreach (Transform box in _boxPositions)
		{
			_boxStock.Add(box, null);
		}
		//AddBeams(_beamPositions.Count);

		_truckOpener.OnTrigger = OpenForTruck;
		_truck.OnEndRun = OnTruckIsLeaving;
		_truck.OnWait = OnTruckIsWaiting; 

		

        yield return null;
        yield return null;
        foreach (WorkerControl workerControl in _playerWorkers) workerControl.gameObject.SetActive(false);

		yield return new WaitForSeconds(10);

		_truck.StartRun();
	}

	private void OnTruckIsWaiting(){
		_truckAlertSign.SetActive(true);
		audioManager.Play(audioManager.klaxonSound);
		audioManager.Play(audioManager.moteurSound);
	}

	private void OnTruckIsLeaving(){
        _lastTruckRun = Time.time;
		audioManager.Stop(audioManager.moteurSound);
	}

	#region Truck
	private void OpenForTruck()
	{
		if (_truck.CurrentStep == TruckScript.ETruckStep.Wait && _truckAlertSign.activeSelf)
		{
			_truckOpener.IsActive = false;
			_truckAlertSign.SetActive(false);
			_truck.MoveNext(DropEquipement);
		}
	}

	private void DropEquipement()
	{
		StartCoroutine(DropEquipement_Coroutine());
	}
	private IEnumerator DropEquipement_Coroutine()
	{
		yield return new WaitForSeconds(1f);
		
		int itemToDrop = Random.Range(_minItemToDrop, _maxItemToDrop + 1);

		int boxToDrop = Mathf.Clamp(0,Random.Range(0, itemToDrop/2),itemToDrop);
		itemToDrop -= boxToDrop;

		int beamToDrop = Mathf.Clamp(0, Random.Range(0, itemToDrop), itemToDrop);
		itemToDrop -= beamToDrop;

		int boardToDrop = Mathf.Clamp(_minItemToDrop, itemToDrop, itemToDrop);

		AddBox(boxToDrop);
		audioManager.Play(audioManager.depotSound);
		yield return new WaitForSeconds(1f);

		AddBeams(beamToDrop);
		audioManager.Play(audioManager.depotSound);
		yield return new WaitForSeconds(1f);

		AddBoard(boardToDrop);
		audioManager.Play(audioManager.depotSound);
		yield return new WaitForSeconds(1f);

		_truck.MoveNext();
		audioManager.Play(audioManager.klaxonSound);
	}

	private void TruckRotation()
	{
		if (_truck.IsRunning || _lastTruckRun + _truckRunDelay > Time.time || _allItems.Count >= _maxItemTotal)
			return;

		_truck.StartRun();
		_truckOpener.IsActive = true;
	}
	#endregion

	// Update is called once per frame
	void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) UnityEngine.SceneManagement.SceneManager.LoadScene (0);

		TruckRotation();

        if(Input.GetKey(KeyCode.O))
		{
			_mainCamera.Scale(_scaleFactor);
		}

		if(Input.GetKey(KeyCode.P))
		{
			_mainCamera.Scale(-_scaleFactor);
		}

		if(Input.GetKey(KeyCode.UpArrow))
		{
			_mainCamera.MoveFront(_moveFactor);
		}
		else if(Input.GetKey(KeyCode.DownArrow))
		{
			_mainCamera.MoveBack(_moveFactor);
		}

		if(Input.GetKey(KeyCode.RightArrow))
		{
			_mainCamera.MoveRight(_moveFactor);
		}
		else if(Input.GetKey(KeyCode.LeftArrow))
		{
			_mainCamera.MoveLeft(_moveFactor);
		}
        //Spawn player
		for(int i = 1; i < 5; i++)
        {
            if(_inputManager.GetButton(InputManager.Button.Start, InputManager.Press.Down, (InputManager.Player)i))
            {
                TogglePlayerSpawn(i);
            }
        }

		if(Input.GetKeyDown(KeyCode.KeypadEnter))
		{
			AddBeams(1);
		}
		else if (Input.GetKeyDown(KeyCode.KeypadPeriod))
		{
			AddBox(1);
		}
		else if(Input.GetKeyDown(KeyCode.Keypad0))
		{
			AddBoard(1);
		}

		CheckHeight();
	}

	#region Adding Items

    private void TogglePlayerSpawn(int playerIndex)
    {
        WorkerControl workerControl = _playerWorkers[playerIndex - 1];

        if (workerControl.gameObject.activeSelf)
        {
            if (!workerControl.IsAirborn)
            {
                workerControl.transform.position = _spawnPoints[playerIndex - 1].position;
                workerControl.gameObject.SetActive(false);
            }
        }
        else
        {
            workerControl.gameObject.SetActive(true);
            workerControl.ReInitialize();
        }
    }

    private void AddPlayer()
	{
		Transform playerSlot = null;

		foreach (KeyValuePair<Transform, bool> slot in _playerSlots)
		{
			if (!slot.Value)
			{
				playerSlot = slot.Key;
			}
		}

		if (playerSlot == null)
		{
			Debug.Log("No more slot");
			return;
		}
		else
		{
			_playerSlots[playerSlot] = true;
		}

		GameObject player = Instantiate(_playerPrefab,playerSlot.position,Quaternion.identity);
	}

    public void AddBeams(float count)
    {
		AddItem(count, _beamPrefab, _beamStock);
	}

    public void AddBox(float count)
    {
		AddItem(count, _boxPrefab, _boxStock);
	}

	public void AddBoard(float count)
    {
		AddItem(count, _boxPrefab, _boardStock);
    }

	private void AddItem(float count, GameObject prefab, Dictionary<Transform,Transform> source)
	{
		if (_allItems.Count >= _maxItemTotal)
			return;

		List<Transform> availableItemSlot = new List<Transform>();
		foreach (KeyValuePair<Transform, Transform> slot in source)
		{
			if (slot.Value == null)
				availableItemSlot.Add(slot.Key);
		}

		for (int i = 0; i < count; i++)
		{
			if (i > availableItemSlot.Count - 1)
			{
				Debug.Log("Stock is full");
				return;
			}

			Transform stock = availableItemSlot[i];
			GameObject item = Instantiate(prefab, stock.position, stock.transform.rotation);
			_allItems.Add(item.transform);
			source[stock] = item.transform;
		}
	}

	private void AddItem(float count, List<GameObject> prefabList, Dictionary<Transform,Transform> source)
	{
		if (_allItems.Count >= _maxItemTotal)
			return;

		List<Transform> availableItemSlot = new List<Transform>();
		foreach (KeyValuePair<Transform, Transform> slot in source)
		{
			if (slot.Value == null)
				availableItemSlot.Add(slot.Key);
		}

		for (int i = 0; i < count; i++)
		{
			if (i > availableItemSlot.Count - 1)
			{
				Debug.Log("Stock is full");
				return;
			}

			Transform stock = availableItemSlot[i];
			GameObject item = Instantiate(prefabList[Random.Range(0, prefabList.Count)], stock.position, stock.transform.rotation);
			_allItems.Add(item.transform);
			source[stock] = item.transform;
		}
	}

	public void PickedUp(Transform item)
	{
		RemoveFromDictionary(_boardStock,item);
		RemoveFromDictionary(_boxStock,item);
		RemoveFromDictionary(_beamStock,item);
	}

	private void RemoveFromDictionary(Dictionary<Transform,Transform> dic, Transform item)
	{
		Transform target = null;
		foreach(KeyValuePair<Transform,Transform> kvp in dic)
		{
			if (kvp.Value == item)
			{
				target = kvp.Key;
				break;
			}
		}

		if (target != null)
			dic[target] = null;
	}
	#endregion

	#region Tower Height
	private void CheckHeight()
	{
		Debug.DrawLine(_raycaster.position, _raycaster.position + Vector3.down * 200);
		RaycastHit hitItem;
		if(Physics.BoxCast(_raycaster.position, _raycaster.localScale, Vector3.down, out hitItem, Quaternion.identity, 200f, itemLayerMask, QueryTriggerInteraction.Collide))
		{
			PickableItemScript item = hitItem.transform.GetComponent<PickableItemScript>();

			if(item.isGrounded)
			{
				_currentTowerHeight = hitItem.point.y;
				_maxHeight = _currentTowerHeight > _maxHeight ? _currentTowerHeight : _maxHeight;
			}

			_currentMaxHeight = hitItem.point.y;

			_uiTowerHeight.text = string.Format("Height : {0:0.00}m",_currentTowerHeight);
			_uiTowerMaxHeight.text = string.Format("Max : {0:0.00}m", _maxHeight);
		}

		if(_scaleForPlayers)
		{
			RaycastHit hitPlayer;
			if (Physics.BoxCast(_raycaster.position, _raycaster.localScale, Vector3.down, out hitPlayer, Quaternion.identity, 200f, playerLayerMask, QueryTriggerInteraction.Collide))
			{
				if (hitPlayer.point.y > _currentMaxHeight)
					_currentMaxHeight = hitPlayer.point.y;
			}
		}
		
		float ratio = Mathf.InverseLerp(_minTowerHeight, _maxTowerHeight, _currentMaxHeight);
		float size = Mathf.Lerp(_minCameraSize, _maxCameraSize, ratio);
		float height = Mathf.Lerp(_minCameraHeight, _maxCameraHeight, ratio);
		_mainCamera.AutoScale(size, height);
	}
	#endregion
}