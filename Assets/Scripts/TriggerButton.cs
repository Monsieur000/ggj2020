﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerButton : MonoBehaviour
{
    public Action OnTrigger;
    public List<string> TargetTags;
    public bool IsActive = true;

    private void OnTriggerEnter(Collider other)
    {
        if(OnTrigger != null && TargetTags.Contains(other.tag) && IsActive)
            OnTrigger.Invoke();
    }
}
