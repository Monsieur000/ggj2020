﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableItemScript : MonoBehaviour{

    public GameObject target;
    public float speedMove;
    public Vector3 hitPoint;

    public Vector3 pointToSnap;
    public bool Fixed;

    private Rigidbody _rig = null;
    private Collider _col = null;

    private Coroutine _positioning = null;

    private const float MULTIPLY_THROW = 10f;

    public bool isGrounded
    {
        get{
            return IsGrounded();
        }
    }

    private void Awake(){
        _rig = GetComponent<Rigidbody>();
        _col = GetComponent<Collider>();
    }

    private void MoveToSnap(){
        transform.SetParent(target.transform);
        StandardPosition();
    }

    public void TakeIt(){
        GameManager.Instance.PickedUp(transform);
        _col.enabled = false;
        _rig.useGravity = false;
        _rig.velocity = Vector3.zero;
        _rig.angularVelocity = Vector3.zero;
        _rig.ResetInertiaTensor();
        _rig.constraints = RigidbodyConstraints.FreezeAll;
        _rig.mass = _rig.mass * 0.5f;
        MoveToSnap();
    }

    public void DropIt(Vector3 fromVelocity){
        _rig.velocity = Vector3.zero;
        _rig.angularVelocity = Vector3.zero;
        _rig.ResetInertiaTensor();
        _rig.constraints = RigidbodyConstraints.None;
        _rig.useGravity = true;
        _col.enabled = true;
        _rig.mass = _rig.mass * 2.0f;
        transform.SetParent(null);
        target = null;
        if (_positioning != null) StopCoroutine(_positioning);
        //_rig.AddForce(fromVelocity * MULTIPLY_THROW);
    }

    /*public void FixIt()
    {
        _rig.constraints = Fixed ? RigidbodyConstraints.None : RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        Fixed = !Fixed;
    }*/

    public void ActivateCollider(){
        _col.enabled = true;
    }

    public void DeactivateCollider(){
        _col.enabled = false;
    }

    private void StandardPosition()
    {
        if (_positioning != null) StopCoroutine (_positioning);
        _positioning = StartCoroutine(StandardPositioning());
    }

    private const float POSITIONING_DURATION = 1f;

    private IEnumerator StandardPositioning()
    {
        float startTime = Time.time;
        Vector3 startPosition = transform.localPosition;
        Quaternion startRotation = transform.localRotation;

        while (Time.time < startTime + POSITIONING_DURATION)
        {
            float t = (Time.time - startTime) / POSITIONING_DURATION;
            transform.localPosition = Vector3.Lerp(startPosition, Vector3.zero, t);
            transform.localRotation = Quaternion.Lerp(startRotation, Quaternion.Euler(0, 90, 0), t);
            yield return null;
        }

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0, 90, 0);

    }

    public bool IsGrounded() {
        int layerMask = 0;
        layerMask += 1 << 0;
        layerMask += 1 << 9;

        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hitInfo;
        bool collide = Physics.Raycast(ray, out hitInfo, GetComponent<Collider>().bounds.size.x/2, layerMask, QueryTriggerInteraction.Collide);
        //if(temp && collide) Debug.Log("Collide " + hitInfo.collider.name);
        return collide;
    }

}
