﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour{

    public static MenuManager mm;

    public AudioManager audioManager;

    public void Awake(){
        mm = this;
        audioManager = AudioManager.Instance;
    }

    public void LaunchScene(string sceneName){
        StartCoroutine(LoadLevelAsyncCoroutine(sceneName));
    }

    private IEnumerator LoadLevelAsyncCoroutine(string sceneName){
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);

        // this line prevents the scene from instant activation
        async.allowSceneActivation = false;

        // activate Scene now
        async.allowSceneActivation = true;

        yield return async;
    }

    public void QuitApplication(){
        Application.Quit();
    }

    public void UI_Select(){
        audioManager.Play(audioManager.selectSound);
    }

    public void UI_Valid(){
        audioManager.Play(audioManager.validSound);
    }

}
