﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckScript : MonoBehaviour
{
	public enum ETruckStep { Start, Wait, Drop, End }

	[SerializeField] Transform _truckStartPos;
	[SerializeField] Transform _truckWaitPos;
	[SerializeField] Transform _dropZonePos;
	[SerializeField] Transform _truckEndPos;

	public bool IsRunning;
	public ETruckStep CurrentStep;
	public Action OnStartRun;
	public Action OnEndRun;
	public Action OnWait;

	private void Awake()
	{
		CurrentStep = ETruckStep.Start;
	}

	public void StartRun()
	{
		if (CurrentStep == ETruckStep.Start)
		{
			IsRunning = true;

			if (OnStartRun != null)
				OnStartRun.Invoke();

			MoveNext(OnWait);
		}
	}
	public void MoveNext(Action callback = null)
	{
		switch(CurrentStep)
		{
			case ETruckStep.Start:
				MoveTruck(_truckWaitPos,5,callback);
				break;
			case ETruckStep.Wait:
				MoveTruck(_dropZonePos,2, callback);
				break;
			case ETruckStep.Drop:
				MoveTruck(_truckEndPos, 5, () => { if(callback != null) callback.Invoke(); Reset(); }) ;
				break;
		}
		CurrentStep++;
	}

	private void Reset()
	{
		transform.position = _truckStartPos.position;
		CurrentStep = 0;
		if (OnEndRun != null)
			OnEndRun.Invoke();
		IsRunning = false;
	}

	private void MoveTruck(Transform destination, float travelTime, Action callback = null)
	{
		LeanTween.move(gameObject, destination, travelTime).setOnComplete(callback);
	}
}
