﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EAxis { x, y, z }
public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera _mainCamera;

    public void MoveFront(float moveFactor)
    {
        Move(moveFactor, EAxis.z);
    }

    public void MoveBack(float moveFactor)
    {
        Move(-moveFactor, EAxis.z);
    }

    public void MoveLeft(float moveFactor)
    {
        Move(-moveFactor, EAxis.x);
    }

    public void MoveRight(float moveFactor)
    {
        Move(moveFactor, EAxis.x);
    }

    public void Move(float moveFactor, EAxis axis)
    {
        Vector3 pos = _mainCamera.transform.localPosition;

        switch (axis)
        {
            case EAxis.x:
                pos.x += moveFactor;
                break;
            case EAxis.y:
                pos.y += moveFactor;
                break;
            case EAxis.z:
                pos.z += moveFactor;
                break;
        }
        
        _mainCamera.transform.localPosition = pos;
    }

    public void Scale(float scaleFactor)
    {
        _mainCamera.orthographicSize += scaleFactor;
    }

    public void AutoScale(float size, float height)
    {
        _mainCamera.orthographicSize = size;
        Vector3 position = transform.position;
        position.y = height;
        transform.position = position;
    }

}