﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerControl : MonoBehaviour
{
    [SerializeField] private InputManager.Player _player = InputManager.Player.One;
    [SerializeField] private Color _playerColor = Color.white;
    [SerializeField] private Rigidbody _rig = null;
    [SerializeField] private Collider _characterCollider = null;

    [SerializeField] private Color[] _playerColors = null;
    [SerializeField] private Material[] _playerMaterials = null;
    [SerializeField] private SkinnedMeshRenderer _mR = null;

    [SerializeField] private float _speed = 1f;
    private Vector3 _lastWalking = Vector3.zero;
    [SerializeField] private AnimationCurve _moveJuiceCurve = null;
    [SerializeField] private float _stopValue = 0.5f;

    private InputManager _inputManager = null;

    private const float MOVE_CONSTANT = 0.1f;
    [SerializeField] private Transform _rotationTransform = null;
    [SerializeField] private float _smoothRotation = 0.5f;

    [SerializeField] private float _jetpackPower = 1f;
    [SerializeField] private float _jetPackDownPower = 1f;
    private float _jetPackPercentSpeed = 0f;
    [SerializeField] private float _jetPackMaxSpeed = 1f;
    [SerializeField] private AnimationCurve _jetPackJuiceCurve = null;
    private float _lastJetPack = -1f;

    private const float CLAMP_ROTATION_AT = 0.001f;

    private bool _isAirborn = false;
    public bool IsAirborn => _isAirborn;
    private const float AIRBORN_DISTANCE = 1.5f;
    private const float MAX_JETPACK_LOCK_MOVE = 0.1f;
    private float _currentJetPackLockMove = 0f;

    private const float CARRYING_MOVE_FACTOR = 0.5f;

    [Header("Animation")]
    [SerializeField] private Animator _animator = null;

    private const string PERCENT_SPEED_FLOAT = "PercentSpeed";
    private const float RUN_SPEED_CONSTANT = 2f;
    private const string AIRBORN_BOOL = "Airborn";
    [SerializeField] private LayerMask _layerMaskAirborn = 0;

    [Header("Action")]
    [SerializeField] private GameObject _handsSpot;
    [SerializeField] private DetectionScript _detection = null;

    private GameObject _itemInHand = null;

    [Header("UiGround")]
    [SerializeField] private WorkerPositionView _workerPositionView = null;

    [Header("FX")]
    [SerializeField] private ParticleSystem[] _jetpackFxs = null;
    [SerializeField] private ParticleSystem _dustFx = null;

    public void Initialize(InputManager.Player player)
    {
        _player = player;
    }

    private void OnEnable()
    {
        _workerPositionView.Initialize(transform, _playerColors[(int)_player - 1]);
        _mR.material = _playerMaterials[(int)_player - 1];
    }

    private IEnumerator Start()
    {
        _inputManager = InputManager.Instance;
        DesactivateJetpackFx();
        yield return null;
        _workerPositionView.Initialize(transform, _playerColors[(int)_player - 1]);
        _mR.material = _playerMaterials[(int)_player - 1];
    }

    private void Update()
    {
        // Attraper un objet
        if (_inputManager.GetButton(InputManager.Button.A, InputManager.Press.Down, _player))
        {
            if (!_itemInHand)
            {
                //Détecter le block que l'on a en face de soi.
                GameObject box = GetItemDetected();
                if (box)
                {
                    _itemInHand = box;

                    Physics.IgnoreCollision(_itemInHand.GetComponent<Collider>(), _characterCollider, true);

                    PickableItemScript pickableItem = _itemInHand.GetComponent<PickableItemScript>();
                    if (null == pickableItem.target)
                    {
                        pickableItem.target = _handsSpot;
                        pickableItem.TakeIt();
                        GameManager.Instance.audioManager.Play(GameManager.Instance.audioManager.attraperObjetSound);
                    }
                }
            }
            else
            {
                Physics.IgnoreCollision(_itemInHand.GetComponent<Collider>(), _characterCollider, false);
                //_itemInHand.GetComponent<MeshRenderer>().material.color = _playerColor;
                _itemInHand.GetComponent<PickableItemScript>().DropIt(_rig.velocity + (_itemInHand.transform.position - transform.position));
                _itemInHand = null;
                GameManager.Instance.audioManager.Play(GameManager.Instance.audioManager.poutreSound);
            }
        }

        /*else if (_inputManager.GetTrigger(InputManager.Joystick.Left,_player) == 1)
        {
            if(!_itemInHand)
            {
                GameObject box = GetItemDetected();
                if(box)
                {
                    box.GetComponent<PickableItemScript>().FixIt();
                }
            }
        }*/
    }

    private GameObject GetItemDetected()
    {
        if (_detection.boxesDetected.Count > 0)
            return _detection.boxesDetected[0];
        else
            return null;
    }

    private void FixedUpdate()
    {
        //Airborn
        bool wasAirborn = _isAirborn;
        CheckAirborn();
        _animator.SetBool(AIRBORN_BOOL, _isAirborn);

        //if (wasAirborn && !_isAirborn) _characterCollider.isTrigger = false;
        //if (!wasAirborn && _isAirborn) _characterCollider.isTrigger = true;

        //Walk
        Vector2 input = _inputManager.GetJoystick(InputManager.Joystick.Left, _player);
        float inputStrenght = (Mathf.Abs(input.x) + Mathf.Abs(input.y)) / 2f;
        Vector3 walking = (_inputManager.MoveReference.right * input.x) + (_inputManager.MoveReference.forward * input.y);
        walking *= _itemInHand ? CARRYING_MOVE_FACTOR : 1f;
        walking = Vector3.Lerp(_lastWalking, walking, _moveJuiceCurve.Evaluate(inputStrenght));
        _lastWalking = walking;

        _animator.SetFloat(PERCENT_SPEED_FLOAT, _itemInHand ? inputStrenght * RUN_SPEED_CONSTANT * 0.5f : inputStrenght * RUN_SPEED_CONSTANT);

        //Jetpack
        float jetPackInput = _inputManager.GetTrigger(InputManager.Joystick.Right, _player);

        if (wasAirborn && !_isAirborn)
        {
            jetPackInput = 0f;
            _jetPackPercentSpeed = 0f;
        }

        bool burst = jetPackInput > 0.25f && jetPackInput >= _lastJetPack;
        if(_lastJetPack < 0.1f && jetPackInput >= 0.1f)
        {
            _dustFx.Play();
            ActivateJetPackFx();
        }

        if (_lastJetPack > 0.05f && jetPackInput <= 0.5f) DesactivateJetpackFx();
        _lastJetPack = jetPackInput;

        _jetPackPercentSpeed = burst ? Mathf.Clamp01(_jetPackPercentSpeed + (_jetpackPower * Time.fixedDeltaTime)) : Mathf.Clamp01(_jetPackPercentSpeed - (_jetPackDownPower * Time.fixedDeltaTime));

        float jetPack = _jetPackJuiceCurve.Evaluate(_jetPackPercentSpeed) * _jetPackMaxSpeed;

        if (_isAirborn) _currentJetPackLockMove = Mathf.Lerp(_currentJetPackLockMove, MAX_JETPACK_LOCK_MOVE, 0.01f);
        else _currentJetPackLockMove = Mathf.Lerp(_currentJetPackLockMove, 0f, 0.01f);
        walking *= _isAirborn ? 1f - _currentJetPackLockMove : 1f;

        bool stayInPlace = false;

        if (_itemInHand != null && !_isAirborn)
        {
            Vector3 checkWalking = walking;
            checkWalking.y = 0f;
            stayInPlace = checkWalking.sqrMagnitude < _stopValue;
        }

        walking = (walking * _speed * MOVE_CONSTANT) + (Vector3.up * jetPack);

        if (walking.y < 0f) walking.y = Mathf.Lerp(walking.y, 0f, jetPackInput > 0.05f ? 0.75f : 0f);

        Vector3 allMove = walking;
        if (stayInPlace)
        {
            allMove.x = Mathf.Lerp(allMove.x, 0f, 0.9f);
            allMove.z = Mathf.Lerp(allMove.y, 0f, 0.9f);
            _animator.SetFloat(PERCENT_SPEED_FLOAT, 0.01f);
        }

        _rig.MovePosition(transform.position + allMove);

        walking.y = 0f;
        float smoothRotation = 1f - _smoothRotation;

        if (inputStrenght >= CLAMP_ROTATION_AT)
        {
            Quaternion targetRotation = Quaternion.LookRotation(walking);
            _rotationTransform.rotation = Quaternion.Slerp(_rotationTransform.rotation, targetRotation, smoothRotation);
        }
    }

    private void CheckAirborn()
    {
        Ray ray = new Ray(transform.position + (Vector3.up * 0.1f), Vector3.down);
        _isAirborn = !Physics.Raycast(ray, out RaycastHit hit, AIRBORN_DISTANCE, _layerMaskAirborn, QueryTriggerInteraction.Collide);
        

        if (Physics.Raycast(ray, out RaycastHit hitB, 200f, _layerMaskAirborn, QueryTriggerInteraction.Collide))
        {
            float percentDistance = Mathf.Clamp01(Vector3.SqrMagnitude(hitB.point - transform.position) / Mathf.Pow(20f, 2f));
            _workerPositionView.SetPosition(hitB.point + (Vector3.up * 0.01f), 1f - percentDistance);

            if (_isAirborn) _workerPositionView.Show();
            else _workerPositionView.Hide();


        }
    }

    private void ActivateJetPackFx()
    {
        foreach(ParticleSystem system in _jetpackFxs)
        {
            system.enableEmission = true;
        }
    }

    private void DesactivateJetpackFx()
    {
        foreach(ParticleSystem system in _jetpackFxs)
        {
            system.enableEmission = false;
        }
    }

    public void ReInitialize()
    {
        _animator.enabled = false;
        _animator.enabled = true;

        _rig.ResetInertiaTensor();
        _rig.velocity = Vector3.zero;
        _rig.angularVelocity = Vector3.zero;
    }

}
