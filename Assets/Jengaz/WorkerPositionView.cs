﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerPositionView : MonoBehaviour
{
    private Transform _worker = null;

    [SerializeField] private SpriteRenderer _view = null;
    [SerializeField] private LineRenderer _line = null;
    [SerializeField] private float _smooth = 0.5f;

    [Range(0f, 1f)]
    [SerializeField] private float _alpha = 0.75f;

    private Color _spriteColor = Color.white;

    public void Initialize(Transform worker, Color color)
    {
        transform.position = worker.position;
        _worker = worker;
        transform.SetParent(null);
        _spriteColor = color;
        _spriteColor.a = _alpha;
        _view.color = _spriteColor;
        _line.startColor = _spriteColor;
        _line.endColor = _spriteColor;
    }

    public void SetPosition(Vector3 position, float size)
    {
        if (_worker != null)
        {
            size *= 0.5f;
            size = Mathf.Clamp(size, 0.2f, 0.5f);
            transform.position = Vector3.Lerp(transform.position, position, 1f - _smooth);
            Vector3 sizeVector = new Vector3(size, size, size);
            transform.localScale = Vector3.Lerp(transform.localScale, sizeVector, 1f - _smooth);
            _line.SetPosition(0, _worker.position + (Vector3.down * 0.25f));
            _line.SetPosition(1, transform.position);
        }
    }

    public void Show()
    {
        
        _view.enabled = true;
        _line.enabled = true;
    }

    public void Hide()
    {
        _view.enabled = false;
        _line.enabled = false;
    }
}
