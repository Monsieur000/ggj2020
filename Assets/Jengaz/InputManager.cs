﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;
    public static bool Exists => Instance != null;

    private void Awake()
    {
        Instance = this;
    }

    public enum Player { One = 1, Two = 2, Three = 3, Four = 4};
    public enum Joystick { Left, Right};
    public enum Axis { Vertical, Horizontal};
    public enum Button { A, B, X, Y, LB, RB, Back, Start};
    public enum Press { Up, Press, Down};

    public Transform MoveReference => transform;

    private const string TRIGGERS = "Triggers";

    private const float DEAD_VALUE = 0.1f;

    private string PlayerString(Player player)
    {
        return string.Format("P{0}_", (int)player);
    }

    private string JoystickString(Joystick joystick)
    {
        return string.Format("Joystick{0}_", joystick.ToString());
    }

    private string AxisString(Axis axis)
    {
        return axis.ToString();
    }

    public Vector2 GetJoystick(Joystick joystick, Player player)
    {
        return new Vector2(GetAxis(Axis.Horizontal, joystick, player), GetAxis(Axis.Vertical, joystick, player));
    }

    public float GetAxis(Axis axis, Joystick joystick, Player player)
    {
        float axisValue = Input.GetAxis(PlayerString(player) + JoystickString(joystick) + AxisString(axis));
        return Mathf.Abs(axisValue) >= DEAD_VALUE ? axisValue : 0f;
    }

    public float GetTrigger(Joystick joystick, Player player)
    {
        switch(joystick)
        {
            case Joystick.Left: return Mathf.Abs(Input.GetAxis(PlayerString(player) + TRIGGERS));
            case Joystick.Right: return Mathf.Clamp01(Input.GetAxis(PlayerString(player) + TRIGGERS));
            default: return 0f;
        }
    }

    public bool GetButton (Button button, Press press, Player player)
    {
        switch(press)
        {
            case Press.Down: return Input.GetKeyDown(ButtonKeycode(button, player));
            case Press.Press: return Input.GetKey(ButtonKeycode(button, player));
            case Press.Up: return Input.GetKeyUp(ButtonKeycode(button, player));
            default: return false;
        }
    }

    private KeyCode ButtonKeycode (Button button, Player player)
    {
        switch(button)
        {
            case Button.A:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button0;
                    case Player.Two: return KeyCode.Joystick2Button0;
                    case Player.Three: return KeyCode.Joystick3Button0;
                    case Player.Four: return KeyCode.Joystick4Button0;
                    default: return KeyCode.None;
                }
            case Button.B:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button1;
                    case Player.Two: return KeyCode.Joystick2Button1;
                    case Player.Three: return KeyCode.Joystick3Button1;
                    case Player.Four: return KeyCode.Joystick4Button1;
                    default: return KeyCode.None;
                }
            case Button.X:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button2;
                    case Player.Two: return KeyCode.Joystick2Button2;
                    case Player.Three: return KeyCode.Joystick3Button2;
                    case Player.Four: return KeyCode.Joystick4Button2;
                    default: return KeyCode.None;
                }
            case Button.Y:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button3;
                    case Player.Two: return KeyCode.Joystick2Button3;
                    case Player.Three: return KeyCode.Joystick3Button3;
                    case Player.Four: return KeyCode.Joystick4Button3;
                    default: return KeyCode.None;
                }
            case Button.LB:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button4;
                    case Player.Two: return KeyCode.Joystick2Button4;
                    case Player.Three: return KeyCode.Joystick3Button4;
                    case Player.Four: return KeyCode.Joystick4Button4;
                    default: return KeyCode.None;
                }
            case Button.RB:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button5;
                    case Player.Two: return KeyCode.Joystick2Button5;
                    case Player.Three: return KeyCode.Joystick3Button5;
                    case Player.Four: return KeyCode.Joystick4Button5;
                    default: return KeyCode.None;
                }
            case Button.Back:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button6;
                    case Player.Two: return KeyCode.Joystick2Button6;
                    case Player.Three: return KeyCode.Joystick3Button6;
                    case Player.Four: return KeyCode.Joystick4Button6;
                    default: return KeyCode.None;
                }
            case Button.Start:
                switch (player)
                {
                    case Player.One: return KeyCode.Joystick1Button7;
                    case Player.Two: return KeyCode.Joystick2Button7;
                    case Player.Three: return KeyCode.Joystick3Button7;
                    case Player.Four: return KeyCode.Joystick4Button7;
                    default: return KeyCode.None;
                }
            default: return KeyCode.None;
        }
    }
}
